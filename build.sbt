name := "betting-risk"

organization := "dk.betex"

version := "0.2-SNAPSHOT"

scalaVersion := "2.10.1"

resolvers ++= Seq(
  "ruippeixotog-snapshots-repo" at "http://maven.ruippeixotog.net/snapshots",
  "dk-snapshots-repo" at "https://dk-maven2-repo.googlecode.com/svn/maven-repo/snapshots",
  "dk-releases-repo" at "https://dk-maven2-repo.googlecode.com/svn/maven-repo/releases")

libraryDependencies ++= Seq(
  "dk.betex" %% "betex-engine" % "0.3-SNAPSHOT",
  "org.apache.commons" % "commons-math" % "2.2",
  "junit" % "junit" % "4.11" % "test",
  "com.novocode" % "junit-interface" % "0.10-M3" % "test"
)

testOptions += Tests.Argument(TestFrameworks.JUnit, "-q", "-v")